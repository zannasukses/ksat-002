import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//open browser
WebUI.openBrowser('')

WebUI.navigateToUrl('https://web.facebook.com/?_rdc=1&_rdr')

WebUI.click(findTestObject('Object Repository/Page_Facebook - Masuk atau Daftar/button_register'))

WebUI.setText(findTestObject('Object Repository/Page_Facebook - Masuk atau Daftar/input_firstName'), 'Test')

WebUI.setText(findTestObject('Object Repository/Page_Facebook - Masuk atau Daftar/input_lastName'), 'Katalon')

//Register using phone number
//WebUI.setText(findTestObject('Object Repository/Page_Facebook - Masuk atau Daftar/input_noHP'), CustomKeywords.'com.ksat002.function.util.StringUtil.getRandomNumber'())

//Register using email and confirmation email
String email = CustomKeywords.'com.ksat002.function.util.StringUtil.getRandomEmail'(15)

WebUI.setText(findTestObject('Object Repository/Page_Facebook - Masuk atau Daftar/input_email'), email)

WebUI.setText(findTestObject('Object Repository/Page_Facebook - Masuk atau Daftar/input_confirm_email'), email)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Facebook - Masuk atau Daftar/input_password'), 'U5R50nI+u/CMmpLW3dRSgg==')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Facebook - Masuk atau Daftar/select_date'), '4', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Facebook - Masuk atau Daftar/select_month'), '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Facebook - Masuk atau Daftar/select_year'), '1996', true)

WebUI.click(findTestObject('Object Repository/Page_Facebook - Masuk atau Daftar/input_gender_F'))

WebUI.click(findTestObject('Object Repository/Page_Facebook - Masuk atau Daftar/button_daftar'))

WebUI.closeBrowser()

