import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.selectOptionByValue(findTestObject('CURA-MakeAppointment/lstFacility'), 'Hongkong CURA Healthcare Center', false)

WebUI.check(findTestObject('CURA-MakeAppointment/chkAdmission'))

WebUI.click(findTestObject('CURA-MakeAppointment/radioProgram'))

//def now = (new Date() + 2).format('dd/MM/yyyy')

WebUI.setText(findTestObject('CURA-MakeAppointment/dateVisit'), CustomKeywords.'com.ksat002.function.util.DateUtil.getToday'())

WebUI.setText(findTestObject('CURA-MakeAppointment/txtComment'), comment + ' new comer')

WebUI.click(findTestObject('CURA-MakeAppointment/btnBookAppointment'))

WebUI.delay(5)

//WebUI.verifyElementText(findTestObject('Object Repository/CURA-ConfirmAppointment/lblFacility'), 'Hongkong CURA Healthcare Center')

String facility = WebUI.getText(findTestObject('Object Repository/CURA-ConfirmAppointment/lblFacility'))

WebUI.verifyMatch(facility, 'Hongkong.*', true)

WebUI.verifyElementText(findTestObject('Object Repository/CURA-ConfirmAppointment/lblHospitalAdmission'), 'Yes')

WebUI.verifyElementText(findTestObject('Object Repository/CURA-ConfirmAppointment/lblProgram'), 'Medicaid')

WebUI.verifyElementText(findTestObject('Object Repository/CURA-ConfirmAppointment/lblVisitDate'), CustomKeywords.'com.ksat002.function.util.DateUtil.getToday'())

WebUI.verifyElementText(findTestObject('Object Repository/CURA-ConfirmAppointment/lblComment'), comment + ' new comer')

WebUI.closeBrowser()