<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>S1 User Function</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>2eef691a-b98b-4329-83a4-86ec57156b7b</testSuiteGuid>
   <testCaseLink>
      <guid>42465888-aa62-48e0-afd1-b8d4422f8105</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e77e3a32-764c-4d4f-b1ba-ae8b5d860913</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/account_list</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>e77e3a32-764c-4d4f-b1ba-ae8b5d860913</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>17f816f2-1f2b-4d9b-b17a-93dbea97e4eb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e77e3a32-764c-4d4f-b1ba-ae8b5d860913</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>8c2181f9-fa89-42bc-a7b5-0db18f0d814c</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
